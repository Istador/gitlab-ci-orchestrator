ARG GOVERSION=1.13.7-alpine3.11
ARG ALPINEVERSION=3.11


########################################################################
###   builder                                                        ###


FROM  golang:$GOVERSION  as  builder

RUN  apk  --no-cache  add  git  gcc  libc-dev

ENV  USER=appuser
ENV  UID=10001
RUN  adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "$UID" \
    "$USER"

WORKDIR  /src/

RUN  go  get  github.com/ungerik/go-rest

COPY  ./  ./

RUN  go  get  -d  -v

ARG  TARGETOS
ARG  TARGETARCH
RUN  GOOS=$TARGETOS  GOARCH=$TARGETARCH  go  build  -ldflags="-w -s"  -o  /build


###   builder                                                        ###
########################################################################
###   runner                                                         ###


FROM  alpine:$ALPINEVERSION  as  runner

ENTRYPOINT  [ "/build" ]
USER  appuser:appuser

ARG  VERSION
ENV  GLCIO_VERSION=$VERSION
ENV  GLCIO_TIMEOUT=1800
ENV  GLCIO_HOST=https://gitlab.com

COPY  --from=builder  /etc/passwd  /etc/passwd
COPY  --from=builder  /etc/group   /etc/group
COPY  --from=builder  /build       /build


###   runner                                                         ###
########################################################################
