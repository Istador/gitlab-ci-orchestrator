package main

import (
  "time"
  "sync"
)

type StampedCacheEntry struct {
  key     string
  expires time.Time
  elem    interface{}
}

type StampedCache struct {
  cache map[string]StampedCacheEntry
  mutex sync.RWMutex // lock for cache
  load  sync.Mutex   // lock for calling f()
  clean *time.Time   // when next cleaning is due
}

func NewStampedCache() *StampedCache {
  obj := StampedCache{}
  obj.cache = make(map[string]StampedCacheEntry)
  obj.clean = nil
  return &obj
}

func (self *StampedCache) Put(
  key  string,
  ttl  time.Duration,
  elem interface{},
) {
  self.Clean()
  self.mutex.Lock()
  defer self.mutex.Unlock()
  self.cache[key] = StampedCacheEntry{
    key     : key,
    expires : time.Now().Add(ttl),
    elem    : elem,
  }
}

func (self *StampedCache) Get(key string) (interface{}, bool) {
  self.mutex.RLock()
  entry, exists := self.cache[key]
  self.mutex.RUnlock()

  if (! exists) { return nil, false }

  // expired?
  if (time.Now().After(entry.expires)) {
    self.mutex.Lock()
    defer self.mutex.Unlock()
    // recheck after lock
    entry, exists = self.cache[key]
    if (! exists) { return nil, false }
    if (time.Now().After(entry.expires)) {
      delete(self.cache, key)
      return nil, false
    }
  }

  return entry.elem, true
}

func (self *StampedCache) GetOrCall(
  key string,
  ttl time.Duration,
  f   func () interface{},
) interface{} {
  elem, exists := self.Get(key)
  if (! exists) {
    // only one process can call f() at the same time
    // (otherwise two processes might call it for the same key at the same time)
    // TODO: replace with a locking mechanism that locks per key, to allow them to run concurrently
    self.load.Lock()
    defer self.load.Unlock()
    // recheck after lock
    elem, exists = self.Get(key)
    if (! exists) {
      elem = f()
      self.Put(key, ttl, elem)
    }
  }
  return elem
}

// clean self.cache every 5 minutes to prevent the memory from running full
func (self *StampedCache) Clean() {
  now := time.Now()
  // first clean
  if (self.clean == nil) {
    next := now.Add(300 * time.Second)
    self.clean = &next
  // time to clean?
  } else if (now.After(*self.clean)) {
    self.mutex.Lock()
    defer self.mutex.Unlock()
    // recheck after lock
    now = time.Now()
    if (now.After(*self.clean)) {
      // for all map entries
      for key, entry := range self.cache {
        // expired?
        if (now.After(entry.expires)) {
          delete(self.cache, key)
        }
      }
      // next clean
      next := now.Add(300 * time.Second)
      self.clean = &next
    }
  }
}
