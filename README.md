## Warning

This is a small Proof of Concept project. Security is not considered, it assumes only complying clients.

Do NOT run it publicly on the internet!

If you want to host it accessible on the internet, you have to add additional security measures.
E.g. a SSH tunnel, or a HTTP proxy with TLS and authorization.

## Orchestrator

This projects offers a small dockerized microservice used to orchestrate the order in which concurrent Gitlab Pipelines do deployments.

See the [example.docker-compose.yml](./example.docker-compose.yml) for an example on how to set it up with `docker-compose`.

### Endpoints

The microservice is runing on port 8080 and offers the following endpoints:
- `/lock/?project=$project_id&pipeline=$pipeline_id&id=$id`: creates a new lock (so that wait is resolved in the right order).
- `/wait/?project=$project_id&pipeline=$pipeline_id&id=$id`: blocks until all previous locks have been removed.
- `/unlock/?project=$project_id&pipeline=$pipeline_id&id=$id`: removes one specific lock.
- `/unlock/all/`: removes all existing locks.
- `/unlock/id/?id=$id`: removes all locks with this id.
- `/get/`: displays all currently active locks.

The `wait` endpoint communicates with the gitlab instance to check if the pipelines are still running.

If `wait` runs longer than 30 minutes it automatically times out with a failure and removes the lock.
This timeout can be changed generally by an environment variable `GLCIO_TIMEOUT`.
The `wait` endpoint also has an optional `timeout=1800` GET parameter, to override the default on a case-by-case basis.

### Environment Variables

The following environment variables exist:
- `GLCIO_HOST`    : defaults to `https://gitlab.com`.
- `GLCIO_TOKEN`   : this is optional if the projects and pipelines are public.
- `GLCIO_TIMEOUT` : integer (seconds), overrides the default timeout of 30 min (1800s).

## lock.sh

To ease using the microservice, this project contains a `lock.sh` that can be copied into the project or onto the runner.

See the [example.gitlab-ci.yml](./example.gitlab-ci.yml) for an example on how to use it.

### Usage

```bash
GLCIO_HOST="http://orchestrator:8080"
id="resource_a"
./lock.sh  lock    $id
./lock.sh  wait    $id
./lock.sh  unlock  $id
```

The `project_id` and `pipeline_id` for the endpoints are automatically extracted from the Gitlab CI environment variables. The script calls the endpoints and analyzes the result from the microservice.

If the second argument for `lock.sh`, the lock identifier `$id`, is omitted, then it locks based on the project and branch for which the pipeline runs.


### Environment Variables

The following environment variable exist:
- `GLCIO_HOST`    : which contains the protocol, domain and (optional) port of the orchestrator. E.g.: `http://orchestrator:8080`.
- `GLCIO_TIMEOUT` : integer (seconds), overrides the default timeout of 30 min (1800s).

`GLCIO_HOST` is required.

### Dependencies

It depends on the following programs to be present on the runner:
- `sh`
- `curl`



