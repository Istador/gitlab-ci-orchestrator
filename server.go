package main

import (
  "os"
  "fmt"
  "log"
  "time"
  "sync"
  "errors"
  "strconv"
  "strings"
  "net/url"
  "net/http"
  "io/ioutil"
  "encoding/json"
  "github.com/ungerik/go-rest"
)

type Entry struct {
  id string
  project string
  pipeline string
}

func indexOf(arr []Entry, e Entry) int {
  for i, v := range arr {
    if (v == e) {
      return i
    }
  }
  return -1
}

func removeFrom(arr []Entry, e Entry) []Entry {
  i := indexOf(arr, e)
  if (i != -1) {
    return append(arr[:i], arr[i+1:]...)
  }
  return arr
}

func entry(in url.Values) Entry {
  return Entry{
    id       : in["id"][0],
    project  : in["project"][0],
    pipeline : in["pipeline"][0],
  }
}

func logEntry(e Entry, msg string) string {
  log.Printf("[%v] %s", e, msg)
  return msg
}

func logError(e Entry, err error, msg string) error {
    log.Printf("[%v] %s %v", e, msg, err)
    return errors.New(msg)
}

type Pipeline struct {
  Status string `json:"status"`
}

type StatusCacheEntry struct {
  Running bool
  Error error
}

/*
 * ask gitlab if the pipeline is still running
 * interpret errors as if it is still running (safe, but might result in blockage)
 */
func raw_is_running(e Entry) (bool, error) {
  host := strings.TrimRight(os.Getenv("GLCIO_HOST"), "/")
  url := host           +
    "/api/v4/projects/" +
    e.project           +
    "/pipelines/"       +
    e.pipeline          +
    "?private_token="   +
    os.Getenv("GLCIO_TOKEN")

  // http get
  resp, err := http.Get(url)
  if (err != nil) {
    return true, logError(e, err, "FAIL_HTTP_GET")
  }
  defer resp.Body.Close()
  body, err := ioutil.ReadAll(resp.Body)
  if (err != nil) {
    return true, logError(e, err, "FAIL_HTTP_READ")
  }

  // parse json
  var pipeline Pipeline
  err = json.Unmarshal([]byte(string(body)), &pipeline)
  if (err != nil) {
    return true, logError(e, err, "FAIL_JSON_PARSE")
  }
  if (pipeline.Status == "") {
    return true, logError(e, err, "FAIL_NO_STATUS")
  }
  return pipeline.Status == "running", nil
}

func main() {
  rest.DontCheckRequestMethod = true
  rest.IndentJSON = "  "
  stopServerChan := make(chan struct{})

  var mutex sync.RWMutex
  data := make(map[string][]Entry)
  status_cache := NewStampedCache()
  status_cache_ttl := 45 * time.Second

  default_timeout, err := strconv.Atoi(os.Getenv("GLCIO_TIMEOUT"))
  if (err != nil) {
    default_timeout = 1800
  }

  head := func (e Entry) *Entry {
    mutex.RLock()
    defer mutex.RUnlock()
    if (len(data[e.id]) < 1) { return nil }
    if (indexOf(data[e.id], e)) == -1 { return nil }
    return &data[e.id][0]
  }

  remove := func (e Entry) {
    mutex.Lock()
    defer mutex.Unlock()
    data[e.id] = removeFrom(data[e.id], e)
    if (len(data[e.id]) == 0) { delete(data, e.id) }
  }

  is_running := func (e Entry) (bool, error) {
    ce := status_cache.GetOrCall(
      e.project + ":" + e.pipeline,
      status_cache_ttl,
      func () interface{} {
        run, err := raw_is_running(e)
        return StatusCacheEntry{run, err}
      },
    ).(StatusCacheEntry)
    return ce.Running, ce.Error
  }

  // background thread to automatically clean up regulary
  cleanup_interval := 3 * time.Hour
  cleanup := func () {
    clean := func () {
      mutex.Lock()
      defer mutex.Unlock()

      // for all queues
      for id, queue := range data {
        for (len(queue) > 0) {
          first := queue[0]
          run, err := is_running(first)
          if (err != nil) { break } // error     => next queue
          if (run) { break }        // running   => next queue
          queue = queue[1:]         // ! running => remove, check next entry
          logEntry(first, "auto cleanup")
        }
        // delete empty queue
        if (len(queue) == 0) { delete(data, id) }
      }
    }

    for {
      time.Sleep(cleanup_interval)
      clean()
    }
  }
  go cleanup() // start thread

  rest.HandleGET("/get/", func() string {
    mutex.RLock()
    defer mutex.RUnlock()
    return fmt.Sprintf("%v", data)
  })

  rest.HandleGET("/lock/", func(in url.Values) string {
    var e = entry(in)

    // add to queue
    mutex.Lock()
    data[e.id] = append(data[e.id], e)
    mutex.Unlock()

    // I know that I'm running now
    status_cache.Put(
      e.project + ":" + e.pipeline,
      status_cache_ttl,
      StatusCacheEntry{true, nil},
    )

    return "OK"
  })

  rest.HandleGET("/wait/", func(in url.Values) string {
    me := entry(in)

    // timeout
    timeout := default_timeout
    if _, ok := in["timeout"]; ok {
      timeout, err = strconv.Atoi(in["timeout"][0])
      if (err != nil) {
        return logEntry(me, "FAIL_INVALID_TIMEOUT")
      }
    }

    i := 0
    for i = 0 ; i < timeout ; i++ {
      // first pipeline that has the lock currently
      first := head(me)

      // no locks exist, abort
      if (first == nil) {
        return logEntry(me, "FAIL_NO_LOCK")
      }

      // I am the first in line, stop waiting
      if (me == *first) { return "OK" }

      // I know that I'm running now
      if (i == 0) {
        status_cache.Put(
          me.project + ":" + me.pipeline,
          status_cache_ttl,
          StatusCacheEntry{true, nil},
        )
      }

      // is the first in line still running? (once a minute)
      if ((i % 60) == 20) {
        run, err := is_running(*first)
        if (err != nil) { return err.Error() }
        if (! run) { remove(*first) }
      }

      // am I still running? (every two minutes)
      if ((i % 120) == 119) {
        run, err := is_running(me)
        if (err != nil) { return err.Error() }
        if (! run) {
          remove(me)
          return logEntry(me, "FAIL_NOT_RUNNING")
        }
      }

      time.Sleep(time.Second)
    }
    return logEntry(me, "FAIL_TIMEOUT")
  })

  rest.HandleGET("/unlock/", func(in url.Values) string {
    var e = entry(in)
    remove(e)
    return "OK"
  })

  rest.HandleGET("/unlock/all/", func() string {
    mutex.Lock()
    data = make(map[string][]Entry)
    mutex.Unlock()
    return "OK"
  })

  rest.HandleGET("/unlock/id/", func(in url.Values) string {
    var id = in["id"][0]
    mutex.Lock()
    delete(data, id)
    mutex.Unlock()
    return "OK"
  })

  rest.RunServer("0.0.0.0:8080", stopServerChan)
}
